```shell
mkdir /usr/live-player & \
cd /usr/live-player & \
git clone https://gitlab.com/codeK-li/nms-player.git . & \
docker run -p 80:80 -v /usr/live-player/dist:/usr/share/nginx/html \
  -v /usr/live-player/nginx:/etc/nginx \
  -d --name live-player nginx
```

# 说明
- 源代码，并不是原始的H5，而是使用了 [parcel](https://parceljs.org/) 进行编译
- 两个主要的文件是：
  - src/index.html
  - src/app.js
- 因为不同的服务器，播放地址是不同的，所以将这部分信息独立到了 [env](https://parceljs.org/features/node-emulation/) 文件中

## 服务器环境
- 播放器的源代码目录为 `/usr/live-player`
- 视频服务器的地址配置在 `/usr/live-player/.env.local`，例如下面的内容：
```
SRS_SERVER=x.y.215.110:8080
```
- 源码目录下的 `dist` 目录为播放器线上代码，由下面命令产生：
```shell
npm run build
```
- 源码目录下的 `nginx` 目录为播放器线上 nginx 的配置文件目录，目前主要是开启了 `gzip` 功能

## 如何提交代码
1. 注册成为 [gitlab](https://gitlab.com/) 用户
2. 成为 [nms-player](https://gitlab.com/codeK-li/nms-player) 的成员
3. 使用 git 提交代码

## 提交代码后，如何在服务上生效
```shell
# 第一步：进入播放器目录
cd /usr/live-player
# 第二步：下载最新的代码
git pull
# 第三步：编译线上代码
npm run build
```