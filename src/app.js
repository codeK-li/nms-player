import 'xgplayer';

const srs_server = process.env.SRS_SERVER;
const srs = {
    flv: `http://${srs_server}/live/livestream.flv`,
    m3u8: `http://${srs_server}/live/livestream.m3u8`,
    playerSelector: '#srs-player'
}

class Features {
    static supportsFlv() {
        return window.MediaSource &&
            window.MediaSource.isTypeSupported('video/mp4; codecs="avc1.42E01E,mp4a.40.2"');
    }
}

class Players {
    static buildFlvPlayer(FlvJsPlayer, conf) {
        new FlvJsPlayer({
            el: document.querySelector(conf.playerSelector), url: conf.flv,
            fluid: true, isLive: true, hasVideo: true, hasAudio: true,
            flvOptionalConfig: {
                enableWorker: true
            },
            airplay: true, lang: 'zh-cn',
            'x5-video-player-fullscreen': true
        });
    }

    static buildHlsPlayer(HlsJsPlayer, conf) {
        new HlsJsPlayer({
            el: document.querySelector(conf.playerSelector), url: conf.m3u8,
            fluid: true, isLive: true,
            airplay: true, lang: 'zh-cn',
            'x5-video-player-fullscreen': true
        });
    }
}

if (Features.supportsFlv()) {
    import('xgplayer-flv.js').then(function (FlvJsPlayer) {
        Players.buildFlvPlayer(FlvJsPlayer, srs);
    }).catch(function (e) {
        alert(e);
    });
} else {
    import('xgplayer-hls.js').then(function (HlsJsPlayer) {
        Players.buildHlsPlayer(HlsJsPlayer, srs);
    }).catch(function (e) {
        alert(e);
    });
}